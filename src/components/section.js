import React from 'react'
import styles from './styles/section.module.css'

const Section = ({ children, className }) => {
  return (
    <div className={className}>
      <div className={styles.pageWrapper}>
        <div className={styles.container}>{children}</div>
      </div>
    </div>
  )
}

export default Section
