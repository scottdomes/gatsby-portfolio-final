import React from 'react'
import styles from './styles/footer.module.css'

const Footer = () => (
  <div className={styles.container}>
    <p className={styles.text}>Copyright 2018.</p>
    <p className={styles.iconCredit}>
      Icon made by{' '}
      <a
        href="http://www.freepik.com/"
        target="_blank"
        rel="noopener noreferrer"
        className={styles.text}
      >
        Freepik
      </a>
      <span> from www.flaticon.com</span>.
    </p>
  </div>
)

export default Footer
