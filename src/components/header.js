import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import styles from './styles/header.module.css'

const Header = ({ logo }) => (
  <div className={styles.container}>
    <Link to="/">
      <div className={styles.logoContainer}>
        <Img className={styles.logo} fixed={logo.fixed} alt="Logo" />
        <h4 className={styles.heading}>
          Scott
          <br />
          Domes
        </h4>{' '}
      </div>
    </Link>
    <div className={styles.linkContainer}>
      <Link to="#projects" className={styles.disappearingLink}>
        Projects
      </Link>
      <Link to="#contact" className={styles.disappearingLink}>
        Contact
      </Link>
      <Link to="/blog" className={styles.link}>
        Blog
      </Link>
    </div>
  </div>
)

export default Header
