import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import 'prismjs/themes/prism-okaidia.css'
import Header from './header'
import Footer from './footer'
import Transition from './transition'
import './layout.css'

const Layout = ({ children, location }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
        logoImage: imageSharp(
          fixed: { originalName: { regex: "/rexy.png/" } }
        ) {
          fixed(width: 40, height: 40) {
            ...GatsbyImageSharpFixed_withWebp
          }
        }
      }
    `}
    render={data => (
      <>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            {
              name: 'description',
              content: 'The portfolio of web developer and author Scott Domes',
            },
            { name: 'keywords', content: 'web development, javascript, ruby' },
          ]}
        >
          <html lang="en" />
        </Helmet>
        <Header logo={data.logoImage} />

        <Transition location={location}>{children}</Transition>
        <Footer />
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
