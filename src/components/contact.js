import React from 'react'
import styles from './styles/contact.module.css'
import Section from './section'

const Contact = () => {
  return (
    <Section>
      <div id="contact" className={styles.titleContainer}>
        <h2 className={styles.title}>Say Hi.</h2>
      </div>
      <form
        className={styles.form}
        name="contact"
        data-netlify="true"
        data-netlify-honeypot="bot-field"
      >
        <input className={styles.honeypot} name="bot-field" />
        <label htmlFor="name">Name</label>
        <input
          id="name"
          className={styles.input}
          name="name"
          placeholder="Your name."
        />
        <label htmlFor="details">Details</label>
        <textarea
          className={styles.textarea}
          name="details"
          id="details"
          placeholder="Pour your heart out."
        />
        <button className={styles.button} type="submit">
          Send!
        </button>
      </form>
    </Section>
  )
}

export default Contact
