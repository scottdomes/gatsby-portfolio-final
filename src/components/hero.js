import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import styles from './styles/hero.module.css'
import Section from './section'

const Hero = ({ image }) => {
  return (
    <Section>
      <div className={styles.container}>
        <div className={styles.imageContainer}>
          <div className={styles.imageWrapper}>
            <Img className={styles.image} fluid={image.fluid} alt="Myself" />
          </div>
        </div>
        <div className={styles.textContainer}>
          <h1 className={styles.bigText}>Let's build something amazing.</h1>
          <hr className={styles.line} />
          <h4>
            Hi, I'm Scott. I'm a React & Ruby on Rails developer, and author of{' '}
            <em>Progressive Web Apps With React</em> and{' '}
            <em>Amazing Websites With React and Gatsby</em>. <br />
            <Link to="#projects" className={styles.link}>
              Check out my work.
            </Link>
          </h4>
        </div>
      </div>
    </Section>
  )
}

export default Hero
