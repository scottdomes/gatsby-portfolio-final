import React from 'react'
import Img from 'gatsby-image'
import styles from './styles/projects.module.css'
import Section from './section'

const projects = [
  {
    title: 'Chatastrophe',
    image: 'project-chatastrophe.png',
    type: 'code',
    link: 'https://github.com/scottdomes/chatastrophe',
  },
  {
    title: 'Progressive Web Apps with React',
    image: 'project-progressive.png',
    type: 'book',
    link:
      'https://www.amazon.com/Progressive-Web-Apps-React-lightning/dp/1788297555',
  },
  {
    title: 'How Flexbox works',
    type: 'article',
    image: 'project-flexbox.png',
    link:
      'https://medium.freecodecamp.org/an-animated-guide-to-flexbox-d280cf6afc35',
  },
]

const Projects = ({ images }) => {
  return (
    <Section className={styles.section}>
      <div id="projects" className={styles.titleContainer}>
        <h2 className={styles.sectionTitle}>Projects</h2>
      </div>
      <div className={styles.projectWrapper}>
        {projects.map(project => {
          const image = images.find(
            img => img.fluid.originalName === project.image
          )
          return (
            <a
              href={project.link}
              target="_blank"
              rel="noopener noreferrer"
              key={project.title}
              className={styles.card}
            >
              {image && <Img fluid={image.fluid} alt={project.title} />}
              <h4 className={styles.title}>{project.title}</h4>
            </a>
          )
        })}
      </div>
    </Section>
  )
}

export default Projects
