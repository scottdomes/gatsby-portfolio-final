import React from 'react'
import styles from './styles/search.module.css'

const Search = ({ value, onChange }) => {
  return (
    <div className={styles.container}>
      <label htmlFor="search" className={styles.label}>
        Search the blog
      </label>
      <input
        className={styles.searchBox}
        type="text"
        id="search"
        onChange={onChange}
        value={value}
      />
    </div>
  )
}

export default Search
