import React from 'react'
import Img from 'gatsby-image'
import styles from './styles/hero.module.css'
import Section from '../section'

const BlogHero = ({ image }) => {
  return (
    <Section>
      <div className={styles.container}>
        <div className={styles.imageContainer}>
          <div className={styles.imageWrapper}>
            <Img className={styles.image} fluid={image.fluid} alt="Myself" />
          </div>
        </div>
        <div className={styles.textContainer}>
          <h4 className={styles.text}>
            Hi, I'm Scott. I'm a React & Ruby on Rails developer, and author of{' '}
            <em>Progressive Web Apps With React</em> and{' '}
            <em>Amazing Websites With React and Gatsby</em>. <br />
            <span className={styles.greenText}>Welcome to my blog!</span>
          </h4>
        </div>
      </div>
    </Section>
  )
}

export default BlogHero
