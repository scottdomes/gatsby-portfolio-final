import React from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import styles from './styles/post_card.module.css'

const PostCard = ({ post, allowFullWidth }) => {
  const { excerpt, frontmatter, timeToRead } = post
  const { cover, title, category, path } = frontmatter
  return (
    <article
      className={`${styles.postContainer} ${
        allowFullWidth ? styles.allowFullWidth : ''
      }`}
    >
      <Link to={path} className={styles.imageContainer}>
        <Img fluid={cover.childImageSharp.fluid} className={styles.cover} />
      </Link>
      <Link to={path} key={title} className={styles.textContainer}>
        <span className={styles.category}>
          {category ? category : 'Web Development'}
        </span>
        <h2 className={styles.title}>{title}</h2>
        <p className={styles.excerpt}>{excerpt}</p>
        <p className={styles.timeToRead}>{timeToRead} Minute Read</p>
      </Link>
    </article>
  )
}

export default PostCard
