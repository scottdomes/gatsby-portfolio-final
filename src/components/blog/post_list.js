import React from 'react'
import styles from './styles/post_list.module.css'
import Section from '../section'
import PostCard from './post_card'

const PostList = ({ posts }) => (
  <Section>
    <div className={styles.container}>
      {posts.map(post => (
        <PostCard post={post} key={post.frontmatter.title} allowFullWidth />
      ))}
    </div>
  </Section>
)

export default PostList
