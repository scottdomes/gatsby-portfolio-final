import React from 'react'
import posed, { PoseGroup } from 'react-pose'

const Container = posed.div({
  enter: { opacity: 1 },
  exit: { opacity: 0 },
})

const TransitionComponent = ({ location = {}, children }) => {
  return (
    <PoseGroup animateOnMount>
      <Container key={location.pathname}>{children}</Container>
    </PoseGroup>
  )
}

export default TransitionComponent
