const { GraphQLScalarType } = require(`graphql`)
const { index } = require('./create_index')
const { SEARCH_INDEX_TYPE } = require('./constants')

const SearchIndex = new GraphQLScalarType({
  name: `${SEARCH_INDEX_TYPE}_Index`,
  description: `Serialized search index`,
  parseValue() {
    throw new Error(`Not supported`)
  },
  serialize(value) {
    return value
  },
  parseLiteral() {
    throw new Error(`Not supported`)
  },
})

exports.setFieldsOnGraphQLNodeType = node => {
  if (node.type.name !== SEARCH_INDEX_TYPE) {
    return null
  }
  return {
    searchIndex: {
      type: SearchIndex,
      resolve: () => index.toJSON(),
    },
  }
}
