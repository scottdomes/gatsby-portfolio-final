const { SEARCH_INDEX_TYPE, SEARCH_INDEX_ID, PARENT } = require('./constants')

exports.createSearchNode = ({ actions }) => {
  const { createNode } = actions
  createNode({
    id: SEARCH_INDEX_ID,
    parent: PARENT,
    children: [],
    internal: {
      type: SEARCH_INDEX_TYPE,
      contentDigest: 'Search',
    },
  })
}
