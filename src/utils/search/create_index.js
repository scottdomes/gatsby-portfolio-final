const elasticlunr = require('elasticlunr')

const index = elasticlunr()
index.addField('title')
index.addField('html') // The name of the content field
index.addField('category')
index.addField('tags')

exports.index = index

exports.addPostToIndex = post => {
  const doc = {
    id: post.frontmatter.path,
    title: post.frontmatter.title,
    category: post.frontmatter.category,
    tags: post.frontmatter.tags,
    html: post.html,
  }
  index.addDoc(doc)
}
