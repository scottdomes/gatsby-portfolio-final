import Typography from 'typography'

const systemFonts = [
  '-apple-system',
  'BlinkMacSystemFont',
  'Segoe UI',
  'Roboto',
  'Helvetica',
  'Arial',
  'sans-serif',
  'Apple Color Emoji',
  'Segoe UI Emoji',
  'Segoe UI Symbol',
]

const typography = new Typography({
  baseFontSize: '18px',
  baseLineHeight: 1.61,
  headerFontFamily: systemFonts,
  bodyFontFamily: systemFonts,
  scaleRatio: 1.618,
  headerWeight: 700,
  bodyWeight: 400,
  boldWeight: 700,
})

const { rhythm, scale } = typography
export { rhythm, scale, typography as default }
