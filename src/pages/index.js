import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'
import Hero from '../components/hero'
import Projects from '../components/projects'
import Contact from '../components/contact'

const IndexPage = ({ data, location }) => (
  <Layout location={location}>
    <Hero image={data.heroImage} />
    <Projects images={data.projectImages.edges.map(edge => edge.node)} />
    <Contact />
  </Layout>
)

export const pageQuery = graphql`
  query ImageQuery {
    heroImage: imageSharp(fluid: { originalName: { regex: "/me.png/" } }) {
      fluid(maxWidth: 1268) {
        ...GatsbyImageSharpFluid_withWebp
      }
    }
    projectImages: allImageSharp(
      filter: { fluid: { originalName: { regex: "/project/" } } }
    ) {
      edges {
        node {
          fluid(maxWidth: 1268) {
            ...GatsbyImageSharpFluid_withWebp
            originalName
          }
        }
      }
    }
  }
`

export default IndexPage
