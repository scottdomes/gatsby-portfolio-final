---
path: "/blog/how-flexbox-works"
title: "How Flexbox works — explained with big, colorful, animated gifs"
cover: cover.png
date: "2017-01-31"
category: "Flexbox"
tags:
  - css
  - flexbox
---

Flexbox’s underlying principle is to make layouts flexible and intuitive.

To accomplish this, it lets containers decide for themselves how to evenly distribute their children — including their size and the space between them.

This all sounds good in principle. But let’s see what it looks like in practice
.
In this article, we’ll dive into the 5 most common Flexbox properties. We’ll explore what they do, how you can use them, and what their results will actually look like.

## Property #1: Display: Flex

Here’s our example webpage:

![1](./1.gif)

You have four colored divs of various sizes, held within a grey container div. As of now, each div has defaulted to `display: block`. Each square thus takes up the full width of its line.

In order to get started with Flexbox, you need to make your container into a flex container. This is as easy as:

```
#container {
  display: flex;
}
```

![2-1](./2.gif)

Not a lot has changed — your divs are displayed inline now, but that’s about it. 

But behind the scenes, you’ve done something powerful. **You gave your squares something called a *flex context*.**

You can now start to position them within that context, with far less difficulty than traditional CSS.

### Property #2: Flex Direction

A Flexbox container has two axes: a main axis and a cross axis, which default to looking like this:
![3](./3.png)

**By default, items are arranged along the main axis, from left to right.** This is why your squares defaulted to a horizontal line once you applied `display: flex`.

`flex-direction`, however, let’s you rotate the main axis.

```
#container {
  display: flex;
  flex-direction: column;
}
```

![4](./4.gif)

There’s an important distinction to make here: `flex-direction: column` doesn’t align the squares on the cross axis instead of the main axis. **It makes the main axis itself go from horizontal to vertical.**

There are a couple of other options for flex-direction, as well: `row-reverse` and `column-reverse`.

![5](./5.gif)

### Property #3: Justify Content

`justify-content` controls how you align items on the **main axis**.

Here, you’ll dive a bit deeper into the main/cross axis distinction. First, let’s go back to `flex-direction: row`.
```
#container {
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
}
```

You have five commands at your disposal to use `justify-content`:
1. Flex-start
1. Flex-end
1. Center
1. Space-between
1. Space-around

![6](./6.gif)

Space-around and space-between are the least intuitive. **Space-between gives equal space between each square, but not between it and the container.**

Space-around puts an equal cushion of space on either side of the square — which means **the space between the outermost squares and the container is half as much as the space between two squares** (each square contributing a non-overlapping equal amount of margin, thus doubling the space).

A final note: remember that **`justify-content` works along the main-axis**, and **`flex-direction` switches the main-axis**. This will be important as you move to…

### Property #4: Align Items

If you ‘get’ justify-content, align-items will be a breeze.

As justify-content works along the main axis, **`align-items` applies to the cross axis.**

![3-1](./7.png)

Let’s reset our `flex-direction` to row, so our axes look the same as the above image.

Then, let’s dive into the align-items commands.

1. flex-start
1. flex-end
1. center
1. stretch
1. baseline


The first three are exactly the same as `justify-content`, so nothing too fancy here.

The next two are a bit different, however.

You have stretch, in which the items take up the entirety of the cross-axis, and baseline, in which the bottom of the paragraph tags are aligned.

![8](./8.gif)

(Note that for `align-items: stretch`, I had to set the height of the squares to auto. Otherwise the height property would override the stretch.)

For `baseline`, be aware that if you take away the paragraph tags, it aligns the bottom of the squares instead, like so:

![9](./9.png)

To demonstrate the main and cross axes better, let’s combine justify-content and align-items and see how centering works different for the two flex-direction commands:

![10](./10.gif)

**With row, the squares are set up along a horizontal main axis. With column, they fall along a vertical main axis.
**
Even if the squares are centered both vertically and horizontally in both cases, the two are not interchangeable!

### Property #5: Align Self

`align-self` allows you to manually manipulate the alignment of one particular element.

It’s basically overriding `align-items` for one square. All the properties are the same, though it defaults to `auto`, in which it follows the `align-items` of the container.
```
#container {
  align-items: flex-start;
}
.square#one {
  align-self: center;
}
// Only this square will be centered.
```

Let’s see what this looks like. You’ll apply `align-self` to two squares, and for the rest apply `align-items: center` and `flex-direction: row`.

![11](./11.gif)

### Conclusion
Even though we’ve just scratched the surface of Flexbox, these commands should be enough for you to handle most basic alignments — and to vertically align to your heart’s content.

Thanks for reading!

<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
      <style type="text/css">
        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; width:100%;}
        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
      </style>
      <div id="mc_embed_signup">
      <form action="//small-habits.us11.list-manage.com/subscribe/post?u=1c1b1269cf6ae05d6c056a081&amp;id=e5992b04cf" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
          <div id="mc_embed_signup_scroll">
        <label for="mce-EMAIL" style="margin: 30px 0;">Get new articles right to your inbox:</label>
        <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1c1b1269cf6ae05d6c056a081_e5992b04cf" tabindex="-1" value=""></div>
          <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
          </div>
      </form>
      </div>

