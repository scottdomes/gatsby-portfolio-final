import React from 'react'
import { Index } from 'elasticlunr'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import Hero from '../components/blog/hero'
import PostList from '../components/blog/post_list'
import Search from '../components/blog/search'

class BlogIndexPage extends React.Component {
  constructor(props) {
    super(props)
    this.index = Index.load(props.data.siteSearchIndex.searchIndex)
    this.state = {
      search: '',
    }
  }

  handleChange = e => {
    const searchInput = e.target.value
    const searchResults = this.index
      .search(searchInput)
      .map(({ ref }) => this.index.documentStore.getDoc(ref).id)
    this.setState({ search: searchInput, searchResults })
  }

  getPosts = data => {
    return data.allMarkdownRemark.edges
      .map(edge => edge.node)
      .filter(this.filterPosts)
  }

  filterPosts = post => {
    if (this.state.searchResults) {
      return this.state.searchResults.includes(post.frontmatter.path)
    }
    return true
  }

  render() {
    const { data, location } = this.props
    return (
      <Layout location={location}>
        <Hero image={data.heroImage} />
        <Search value={this.state.search} onChange={this.handleChange} />
        <PostList posts={this.getPosts(data)} />
      </Layout>
    )
  }
}

export const pageQuery = graphql`
  query BlogQuery {
    siteSearchIndex {
      searchIndex
    }
    heroImage: imageSharp(fluid: { originalName: { regex: "/me.png/" } }) {
      fluid(maxWidth: 1268) {
        ...GatsbyImageSharpFluid_withWebp
      }
    }
    allMarkdownRemark(
      limit: 20
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { path: { regex: "/blog/" } } }
    ) {
      edges {
        node {
          excerpt
          timeToRead
          frontmatter {
            title
            category
            cover {
              childImageSharp {
                fluid(maxWidth: 968) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            path
          }
        }
      }
    }
  }
`

export default BlogIndexPage
