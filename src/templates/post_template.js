import React from 'react'
import { graphql, Link } from 'gatsby'
import Img from 'gatsby-image'
import styles from './styles/post_template.module.css'
import Layout from '../components/layout'
import PostCard from '../components/blog/post_card'

const PostTemplate = ({ data, location }) => {
  const { markdownRemark, allMarkdownRemark } = data
  const { frontmatter, html } = markdownRemark
  const posts = allMarkdownRemark.edges.filter(
    edge => edge.node.frontmatter.title !== frontmatter.title
  )

  return (
    <Layout location={location}>
      <div className={styles.container}>
        <div className={styles.card}>
          <div className={styles.padder}>
            <h1 className={styles.heading}>{frontmatter.title}</h1>
          </div>
          {frontmatter.cover && (
            <Img
              fluid={frontmatter.cover.childImageSharp.fluid}
              alt="Cover"
              className={styles.cover}
            />
          )}
          <div className={styles.padder}>
            <p>
              Tags:{' '}
              {frontmatter.tags.map(tag => (
                <Link key={tag} className={styles.tag} to={`/tags/${tag}`}>
                  {tag}
                </Link>
              ))}
            </p>
            <div dangerouslySetInnerHTML={{ __html: html }} />
          </div>
        </div>
        <div className={styles.relatedPostContainer}>
          <div className={styles.relatedPostContainer}>
            <div className={styles.relatedPostWrapper}>
              {posts.map(edge => (
                <PostCard
                  post={edge.node}
                  key={edge.node.frontmatter.title}
                  className={styles.post}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query PostQuery($path: String!, $relatedTags: [String]) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        title
        cover {
          childImageSharp {
            fluid(maxWidth: 968) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
        tags
      }
    }
    allMarkdownRemark(
      limit: 3
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: $relatedTags } } }
    ) {
      edges {
        node {
          excerpt
          timeToRead
          frontmatter {
            title
            category
            cover {
              childImageSharp {
                fluid(maxWidth: 968) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            path
          }
        }
      }
    }
  }
`

export default PostTemplate
