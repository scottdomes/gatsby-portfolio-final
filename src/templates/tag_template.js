import React from 'react'
import { Link, graphql } from 'gatsby'
import Layout from '../components/layout'
import styles from './styles/tag_template.module.css'
import PostList from '../components/blog/post_list'

const TagTemplate = ({ data, pageContext, location }) => {
  return (
    <Layout location={location}>
      <div className={styles.headingContainer}>
        <h1>
          <span className={styles.greenText}>Tags</span> /{' '}
          <span className={styles.greenText}>{pageContext.tag}</span>
        </h1>
        <h4>
          {pageContext.tags.map(tag => (
            <Link key={tag} className={styles.link} to={`/tags/${tag}`}>
              {tag}
            </Link>
          ))}
        </h4>
      </div>
      <PostList posts={data.allMarkdownRemark.edges.map(edge => edge.node)} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query TagQuery($tag: String!) {
    allMarkdownRemark(
      limit: 20
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      edges {
        node {
          excerpt
          timeToRead
          frontmatter {
            title
            category
            cover {
              childImageSharp {
                fluid(maxWidth: 968) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            path
          }
        }
      }
    }
  }
`

export default TagTemplate
