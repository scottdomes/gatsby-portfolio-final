const path = require('path')
const searchIndex = require('./src/utils/search/create_index')
const searchNode = require('./src/utils/search/node')
const searchGraphQL = require('./src/utils/search/graphql')

exports.sourceNodes = searchNode.createSearchNode
exports.setFieldsOnGraphQLNodeType = searchGraphQL.setFieldsOnGraphQLNodeType

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  const postTemplate = path.resolve(`src/templates/post_template.js`)
  const tagTemplate = path.resolve(`src/templates/tag_template.js`)

  return graphql(`
    {
      allMarkdownRemark(limit: 1000) {
        edges {
          node {
            html
            frontmatter {
              path
              tags
              title
              category
              tags
            }
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors)
    }

    const tagSet = new Set()

    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
      if (node.frontmatter.tags) {
        node.frontmatter.tags.forEach(tag => tagSet.add(tag))
      }

      searchIndex.addPostToIndex(node)

      createPage({
        path: node.frontmatter.path,
        component: postTemplate,
        context: {
          relatedTags: node.frontmatter.tags,
        },
      })
    })

    tagSet.forEach(tag => {
      createPage({
        component: tagTemplate,
        path: `/tags/${tag}/`,
        context: {
          tag,
          tags: Array.from(tagSet),
        },
      })
    })
  })
}
